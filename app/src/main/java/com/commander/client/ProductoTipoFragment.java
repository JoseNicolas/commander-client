package com.commander.client;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import adapters.ProductoTipoAdapter;
import model.ClientTask;
import model.ProductoTipo;
import model.Request;
import model.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProductoTipoFragment extends Fragment implements ClientTask.OnTaskCompleted{

    View view;
    MainActivity main;
    Bundle bundle;
    ProgressBar progress_bar;
    RecyclerView recycler;
    ProductoTipoAdapter adapter;
    List<ProductoTipo> producto_tipos;
    public static boolean come_from_productos;
    private GridLayoutManager glm;

    public ProductoTipoFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_producto_tipo, container, false);

        try{
            if (come_from_productos){
                come_from_productos = false;
            }else{
                checkComandaExisting(view);
            }

            main = (MainActivity) getActivity();

            progress_bar = (ProgressBar) view.findViewById(R.id.pb_producto_tipos);
            recycler = (RecyclerView) view.findViewById(R.id.rv_producto_tipos);

            bundle = this.getArguments();
//            user_logged = (Usuario) bundle.getSerializable("user_logged");
            glm = new GridLayoutManager(view.getContext(), 3);
            recycler.setLayoutManager(glm);

            getProductoTipos();

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    /**
     * Get list of ProductoTipo from server
     */
    private void getProductoTipos(){
        Request request = new Request();
        request.setAction("readAll-productoTipo");
        request.setData(null);
        ClientTask m = new ClientTask("readAll-productoTipo", request, this, getContext());
        m.execute();
    }

    /**
     * ClientTask complete task. Response from server.
     * @param response
     */
    @Override
    public void onTaskCompleted(String task_id, final Response response) {
        try{
            main.runOnUiThread(new Runnable() {
                public void run() {
                    try{
                        progress_bar.setVisibility(View.GONE);

                        if (response.isStatus() && !response.isError()){

                            try{
                                producto_tipos = (List<ProductoTipo>) response.getData();
                                adapter = new ProductoTipoAdapter(producto_tipos);
                                recycler.setAdapter(adapter);

                            }catch (Exception e){
                                Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(view.getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            main.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(view.getContext(), "Se produjo un error inesperado", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Comprueba si ya existe
     * @param view
     */
    public void checkComandaExisting(View view){
        if (MainActivity.detalle_comanda != null){
            new AlertDialog.Builder(view.getContext())
                .setIcon(R.drawable.bandeja)
                .setTitle("Comanda en curso")
                .setMessage("Existe una comanda en curso. ¿Crear nueva o continuar la comanda?")
                .setPositiveButton("Nueva", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.detalle_comanda = null;
                    }

                })
                .setNegativeButton("Continuar", null)
                    .show();
        }
    }
}
