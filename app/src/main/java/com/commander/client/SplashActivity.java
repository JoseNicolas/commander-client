package com.commander.client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.rbddevs.splashy.Splashy;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setSplashy();
    }

    private void setSplashy(){
        Splashy s = new Splashy(this);
        s.setLogo(R.drawable.bandeja)
            .setTitle("Commander APP")
            .setTitleColor("#D87625")
            .setSubTitle("BETA Version")
            .setProgressColor(R.color.white)
            .setBackgroundResource(0)
            .setFullScreen(true)
            .setTime(1000)
            .show();

        Splashy.Companion.onComplete(new Splashy.OnComplete(){
            @Override
            public void onComplete() {
//                Toast.makeText(SplashActivity.this, "Logged In", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
