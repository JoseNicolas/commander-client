package com.commander.client;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import adapters.ComandaAdapter;
import model.Comanda;
import model.Mesa;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComandaHistoricoFragment extends Fragment {

    View view;
    MainActivity main;
    ImageView iv_empty_history;
    ProgressBar progress_bar;
    RecyclerView rv_comanda;

    List<Mesa> lista_mesas;
    List<Comanda> lista_comandas;
    ComandaAdapter adapter;
    private LinearLayoutManager llm;

    public ComandaHistoricoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comanda_historico, container, false);

        main = (MainActivity) getActivity();
        main.getBaseData();

        iv_empty_history = view.findViewById(R.id.iv_empty_history);
        rv_comanda = view.findViewById(R.id.rv_comanda);
        progress_bar = view.findViewById(R.id.pb_comanda_historico);

        lista_mesas = MainActivity.mesas;
        lista_comandas = MainActivity.comandas;

        if (lista_comandas != null){
            if (lista_comandas.size() > 0){
                iv_empty_history.setVisibility(View.GONE);
                progress_bar.setVisibility(View.GONE);

                llm = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
                rv_comanda.setLayoutManager(llm);
                adapter = new ComandaAdapter(lista_comandas, lista_mesas);
                rv_comanda.setAdapter(adapter);

            }else{
                Toast.makeText(getActivity(), "No se encontró histórico de comandas", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getActivity(), "No se encontró histórico de comandas", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

}
