package com.commander.client;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import model.Client;
import model.ClientTask;
import model.Request;
import model.Response;
import model.Usuario;


public class LoginActivity extends AppCompatActivity {

    Button bt_acceder;
    EditText et_usuario;
    EditText et_password;
    ProgressBar progress_bar;
    public Fragment active_fragment;
    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        active_fragment = new LoginFragment();
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container_login, active_fragment).commit();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (active_fragment instanceof PreferenciasFragment){
            active_fragment = new LoginFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container_login, active_fragment).commit();
        }else{
            confirmExit();
        }
    }

    public void confirmExit(){
        try{
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.bandeja)
                    .setTitle("SALIR")
                    .setMessage("¿Está seguro que desea salir de la aplicación?")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }

                    })
                    .setNegativeButton("cancelar", null)
                    .show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
