package com.commander.client;


import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import adapters.DetalleComandaAdapter;
import model.ClientTask;
import model.Comanda;
import model.DetalleComanda;
import model.Mesa;
import model.Producto;
import model.Request;
import model.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComandaCursoFragment extends Fragment implements ClientTask.OnTaskCompleted, View.OnClickListener {

    View view;
    MainActivity main;
    TextView tv_mesa, tv_detalle_comanda;
    ImageView iv_empty_tray;
    Spinner sp_mesas;
    ProgressBar progress_bar;
    RecyclerView rv_detalle_comanda;
    Button bt_eliminar_comanda;
    Button bt_enviar_comanda;
    ScrollView scroll;

    List<Mesa> lista_mesas;
    List<DetalleComanda> lista_detalle_comanda;
    List<Producto> lista_productos;
    DetalleComandaAdapter adapter;
    private LinearLayoutManager llm;

    public ComandaCursoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comanda_curso, container, false);

        main = (MainActivity) getActivity();
        iv_empty_tray = view.findViewById(R.id.iv_empty_tray);
        tv_mesa = view.findViewById(R.id.tv_mesa);
        tv_detalle_comanda = view.findViewById(R.id.tv_detalle_comanda);
        sp_mesas = view.findViewById(R.id.sp_mesas);
        rv_detalle_comanda = view.findViewById(R.id.rv_detalle_comanda);
        progress_bar = view.findViewById(R.id.pb_comanda_curso);
        bt_eliminar_comanda = view.findViewById(R.id.bt_eliminar_comanda);
        bt_enviar_comanda = view.findViewById(R.id.bt_enviar_comanda);
        bt_eliminar_comanda.setOnClickListener(this);
        bt_enviar_comanda.setOnClickListener(this);
        scroll = view.findViewById(R.id.scrollview_comanda_curso);

        lista_mesas = MainActivity.mesas;
        lista_detalle_comanda = MainActivity.detalle_comanda;

        if (lista_mesas != null){
            if (lista_mesas.size() > 0){
                ArrayList<String> arrayList = new ArrayList<>();
                for (int i=0; i<lista_mesas.size(); i++){
                    arrayList.add(lista_mesas.get(i).getNombre());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_item, arrayList);
                sp_mesas.setAdapter(arrayAdapter);
            }
        }

        if (lista_detalle_comanda != null){
            if (lista_detalle_comanda.size() > 0){
                toggleShowComponents(true);
                iv_empty_tray.setVisibility(View.GONE);

                llm = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
                rv_detalle_comanda.setLayoutManager(llm);

                getProductos();
            }else{
                toggleShowComponents(false);
                Toast.makeText(getActivity(), "No existe ninguna comanda en curso", Toast.LENGTH_SHORT).show();
            }
        }else{
            toggleShowComponents(false);
            Toast.makeText(getActivity(), "No existe ninguna comanda en curso", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    @Override
    public void onTaskCompleted(String task_id, Response response) {
        try{
            main.runOnUiThread(new Runnable() {
                public void run() {
                    try{
                        progress_bar.setVisibility(View.GONE);

                        if (response.isStatus() && !response.isError()){

                            switch (task_id){
                                case "get-productos":
                                    try{
                                        lista_productos = (List<Producto>) response.getData();
                                        adapter = new DetalleComandaAdapter(lista_detalle_comanda, lista_productos);
                                        rv_detalle_comanda.setAdapter(adapter);

                                    }catch (Exception e){
                                        Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                case "set-comanda":
                                    MainActivity.detalle_comanda = null;
                                    final FragmentTransaction transaction = main.getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.main_container, new ComandaFragment()).commit();
                                    break;
                            }
                        }else{
                            Toast.makeText(view.getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            main.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(view.getContext(), "Se produjo un error inesperado", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        final FragmentTransaction transaction = main.getFragmentManager().beginTransaction();

        switch (v.getId()){
            case R.id.bt_enviar_comanda:
                createComanda(v);
                break;
            case R.id.bt_eliminar_comanda:
                deleteComanda(v);
                break;
        }
    }

    /**
     * Get list of Productos from server
     */
    private void getProductos(){
        Request request = new Request();
        request.setAction("readAll-producto");
        request.setData(null);
        ClientTask m = new ClientTask("get-productos", request, this, getContext());
        m.execute();
    }

    /**
     * Elimina la comanda en curso
     */
    private void deleteComanda(View v){
        final FragmentTransaction transaction = main.getFragmentManager().beginTransaction();

        try{
            new AlertDialog.Builder(v.getContext())
                    .setIcon(R.drawable.ic_delete)
                    .setTitle("Eliminar comanda en curso")
                    .setMessage("¿Está seguro de eliminar la comanda en curso?")
                    .setPositiveButton("Eliminar", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.detalle_comanda = null;

                            transaction.replace(R.id.main_container, new ComandaFragment()).commit();
                        }

                    })
                    .setNegativeButton("Cancelar", null)
                    .show();

        }catch (Exception e){
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Crea una nueva comanda
     * @param v
     */
    private void createComanda(View v){
        try{
            new AlertDialog.Builder(v.getContext())
                    .setIcon(R.drawable.ic_send_dark)
                    .setTitle("Enviar comanda")
                    .setMessage("Se va a proceder a enviar la comanda al servidor. ¿Enviar comanda?")
                    .setPositiveButton("Enviar", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendComanda();
                        }

                    })
                    .setNegativeButton("Cancelar", null)
                    .show();
        }catch (Exception e){
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Envia una nueva comanda al servidor.
     */
    private void sendComanda(){
        Request request;
        ClientTask c;
        List data;
        Comanda comanda;
        Mesa mesa;
        List<DetalleComanda> detalle_comanda;

        try{
            if (validarComanda()){
                String sp_mesa = (String) sp_mesas.getSelectedItem();
                mesa = lista_mesas.stream().filter(m -> m.getNombre() == sp_mesa).findAny().get();
                data = new ArrayList();
                comanda = new Comanda();
                detalle_comanda = lista_detalle_comanda;

                comanda.setMesa(mesa.getId());
                comanda.setCamarero(MainActivity.user_logged.getId());
                comanda.setEstado("pendiente");
                comanda.setCreated(new Date());
                comanda.setCreator(MainActivity.user_logged.getId());
                comanda.setUpdated(new Date());
                comanda.setUpdater(MainActivity.user_logged.getId());

                data.add(0, comanda);
                data.add(1, detalle_comanda);

                request = new Request();
                request.setAction("create_detalle-comanda");
                request.setData(data);
                c = new ClientTask("set-comanda", request, this, getContext());
                c.execute();
            }
        }catch (Exception e){
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Valida que la informacion para el envio de la comanda sea la necesaria.
     * @return
     */
    private boolean validarComanda(){
        boolean is_valid = false;

        if (lista_detalle_comanda != null){
            if (lista_detalle_comanda.size() > 0){

                String sp_mesa = (String) sp_mesas.getSelectedItem();
                if (sp_mesa != null){
                    if (sp_mesa != ""){

                        Optional<Mesa> mesa_exist = lista_mesas.stream().filter(m -> m.getNombre() == sp_mesa).findAny();
                        if (mesa_exist.isPresent()){

                            is_valid = true;

                        }else{
                            Toast.makeText(view.getContext(), "Seleccione una mesa para la comanda", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(view.getContext(), "Seleccione una mesa para la comanda", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(view.getContext(), "Seleccione una mesa para la comanda", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(view.getContext(), "Añada productos a la comanda", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(view.getContext(), "Añada productos a la comanda", Toast.LENGTH_SHORT).show();
        }

        return is_valid;
    }

    /**
     * Muestra y/o oculta los componentes de la vista
     * @param show
     */
    private void toggleShowComponents(boolean show){
        int action = View.GONE;

        if (show){
            action = View.VISIBLE;
        }

        progress_bar.setVisibility(action);
        tv_mesa.setVisibility(action);
        sp_mesas.setVisibility(action);
        tv_detalle_comanda.setVisibility(action);
        rv_detalle_comanda.setVisibility(action);
        bt_enviar_comanda.setVisibility(action);
        bt_eliminar_comanda.setVisibility(action);
    }
}
