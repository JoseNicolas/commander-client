package com.commander.client;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapters.ProductoAdapter;
import model.ClientTask;
import model.ClientTaskImage;
import model.DetalleComanda;
import model.Producto;
import model.Request;
import model.Response;
import model.Usuario;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProductoFragment extends Fragment implements ClientTask.OnTaskCompleted, ClientTaskImage.OnTaskImageCompleted, View.OnClickListener {

    View view;
    MainActivity main;
    Bundle bundle;
    Usuario user_logged;
    FloatingActionButton fab_enviar;
    ProgressBar progress_bar;
    RecyclerView recycler;
    ProductoAdapter adapter;
    List<Producto> productos;
    List<Bitmap> productos_images;
    private LinearLayoutManager llm;
    private String id_producto_tipo;

    public ProductoFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_producto, container, false);

        try{
            main = (MainActivity) getActivity();

            fab_enviar = (FloatingActionButton) view.findViewById(R.id.fab_enviar);
            fab_enviar.setOnClickListener(this);
            progress_bar = (ProgressBar) view.findViewById(R.id.pb_productos);
            recycler = (RecyclerView) view.findViewById(R.id.rv_productos);

            bundle = this.getArguments();
            user_logged = (Usuario) bundle.getSerializable("user_logged");
            id_producto_tipo = bundle.getString("id_producto_tipo");
            llm = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
            recycler.setLayoutManager(llm);

            if (id_producto_tipo != null){
                getProductos(id_producto_tipo);
            }else{
                Toast.makeText(getActivity(), "Debe seleccionar un tipo de producto previamente", Toast.LENGTH_SHORT).show();
                main.onBackPressed();
            }


        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    /**
     * Get list of Producto from server
     */
    private void getProductos(String id_producto_tipo){
        Request request = new Request();
        request.setAction("readAllByTipo-producto");
        request.setData(id_producto_tipo);
        ClientTask m = new ClientTask("readAllByTipo-producto", request, this, getContext());
        m.execute();
    }

    @Override
    public void onTaskCompleted(String task_id, final Response response) {
        try{
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try{
                        progress_bar.setVisibility(View.GONE);

                        if (response.isStatus() && !response.isError()){

                            try{
                                productos_images = new ArrayList<>();
                                productos = (List<Producto>) response.getData();

                                if (productos != null){
                                    adapter = new ProductoAdapter(productos, productos_images);
                                    recycler.setAdapter(adapter);

                                    for (int i=0; i<productos.size(); i++){
                                        Producto p = productos.get(i);
                                        ClientTaskImage c = new ClientTaskImage(String.valueOf(p.getId()), ProductoFragment.this);
                                        c.execute();
                                    }

                                }
                            }catch (Exception e){
                                Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(view.getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(view.getContext(), "Se produjo un error inesperado", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onTaskImageCompleted(Bitmap bitmap) {
        productos_images.add(bitmap);

        if (getActivity() == null){
            return;
        }
        getActivity().runOnUiThread(new Runnable(){
            public void run() {
                adapter.notifyDataSetChanged();
                }
            });
    }

    @Override
    public void onClick(View v) {
        final FragmentTransaction transaction = main.getFragmentManager().beginTransaction();

        if (v.getId() == fab_enviar.getId()){
            List<DetalleComanda> detalle_comanda = MainActivity.detalle_comanda;

            if (detalle_comanda != null){
                if (detalle_comanda.size() > 0){

                    transaction.replace(R.id.main_container, new ComandaFragment()).commit();

                }else{
                    Toast.makeText(getActivity(), "Añada al menos 1 producto a la comanda", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(getActivity(), "No existe ninguna comanda definida", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
