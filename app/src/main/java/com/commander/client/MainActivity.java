package com.commander.client;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;


import model.ClientTask;
import model.Comanda;
import model.DetalleComanda;
import model.Mesa;
import model.Request;
import model.Response;
import model.Usuario;

public class MainActivity extends AppCompatActivity implements ClientTask.OnTaskCompleted {

    private final String TITLE = "Commander BETA";
    public static Fragment active_fragment;
    private Bundle bundle;
    private BottomNavigationView navigation;
    public static List<DetalleComanda> detalle_comanda;
    public static Usuario user_logged;
    public static List<Mesa> mesas;
    public static List<Comanda> comandas;

    ProgressBar progress_bar;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try{
            detalle_comanda = null;

            navigation = (BottomNavigationView) findViewById(R.id.navigation);
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle(TITLE);
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);

            progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
            progress_bar.setVisibility(View.GONE);

            bundle = getIntent().getExtras();
            user_logged = (Usuario) bundle.getSerializable("user_logged");

            getBaseData();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;

            try{
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle b = new Bundle();
                b.putSerializable("user_logged", user_logged);

                switch (item.getItemId()) {
                    case R.id.navigation_comandas:
                        toolbar.setTitle("Mis Comandas");
                        fragment = new ComandaFragment();
                        fragment.setArguments(b);
                        break;
                    case R.id.navigation_nueva:
                        toolbar.setTitle("Tipo de producto");
                        fragment = new ProductoTipoFragment();
                        fragment.setArguments(b);
                        break;
                    case R.id.navigation_settings:
                        toolbar.setTitle("Ajustes de Aplicación");
                        fragment = new PreferenciasFragment();
                        fragment.setArguments(b);
                        break;
                    default:
                        toolbar.setTitle("Mis Comandas");
                        fragment = new ComandaFragment();
                        fragment.setArguments(b);
                        break;
                }

                if (active_fragment == null){
                    active_fragment = new Fragment();
                    fragment = new ComandaFragment();
                }

                if (active_fragment.getClass() != fragment.getClass()){
                    transaction.replace(R.id.main_container, fragment).commit();
                    active_fragment = fragment;
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            return true;
        }
    };

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

            if ( active_fragment instanceof ComandaFragment ){
                confirmExit();            }
            else if ( active_fragment instanceof ProductoTipoFragment ){
                navigation.setSelectedItemId(R.id.navigation_comandas);
            }
            else if ( active_fragment instanceof ProductoFragment ){
                ProductoTipoFragment.come_from_productos = true;
                navigation.setSelectedItemId(R.id.navigation_nueva);
            }
            else {
                navigation.setSelectedItemId(R.id.navigation_comandas);
            }
    }

    @Override
    public void onTaskCompleted(String task_id, final Response response) {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    progress_bar.setVisibility(View.GONE);

                    if (response.isStatus() && !response.isError()) {

                        try {
                            List lista = (List) response.getData();

                            mesas = (List<Mesa>) lista.get(0);
                            comandas = (List<Comanda>) lista.get(1);

                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    navigation.setSelectedItemId(R.id.navigation_comandas);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Obtiene la informacion base de la aplicacion. (Comandas y mesas)
     */
    public void getBaseData(){
        Request r = new Request();
        r.setAction("client-base_data");
        r.setData(user_logged);
        ClientTask c = new ClientTask("client-base_data", r, this, getBaseContext());
        c.execute();
    }

    public void confirmExit(){
        try{
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.bandeja)
                    .setTitle("SALIR")
                    .setMessage("¿Está seguro que desea salir de la aplicación?")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }

                    })
                    .setNegativeButton("cancelar", null)
                    .show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
