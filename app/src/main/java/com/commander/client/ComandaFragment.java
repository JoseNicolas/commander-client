package com.commander.client;


import android.content.res.Resources;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComandaFragment extends Fragment implements TabHost.OnTabChangeListener{

    View view;
    Resources res;

    public ComandaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comanda, container, false);

        res = getResources();

        TabHost tabs=(TabHost)view.findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("tab_comanda_curso");
        spec.setContent(R.id.tab_comanda_curso);
        spec.setIndicator("EN CURSO", res.getDrawable(android.R.drawable.ic_btn_speak_now));
        tabs.addTab(spec);

        spec=tabs.newTabSpec("tab_comanda_historico");
        spec.setContent(R.id.tab_comanda_historico);
        spec.setIndicator("HISTÓRICO",
                res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);

        // Pone el 1er tab por defecto
        tabs.setCurrentTab(0);
        getFragmentManager().beginTransaction()
                .replace(R.id.tab_comanda_curso, new ComandaCursoFragment())
                .commit();

        tabs.setOnTabChangedListener(this);

        return view;
    }

    @Override
    public void onTabChanged(String tabId) {
        switch (tabId){
            case "tab_comanda_curso":
                getFragmentManager().beginTransaction()
                        .replace(R.id.tab_comanda_curso, new ComandaCursoFragment())
                        .commit();
                break;
            case "tab_comanda_historico":
                getFragmentManager().beginTransaction()
                        .replace(R.id.tab_comanda_historico, new ComandaHistoricoFragment())
                        .commit();
                break;
        }
    }
}
