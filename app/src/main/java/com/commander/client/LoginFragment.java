package com.commander.client;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import model.ClientTask;
import model.Request;
import model.Response;
import model.Usuario;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, ClientTask.OnTaskCompleted{

    private final String TITLE = "Commander | Autentificación";

    Button bt_acceder;
    EditText et_usuario;
    EditText et_password;
    ProgressBar progress_bar;
    ImageView iv_settings;
    LoginActivity main;
    View view;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);

        main = (LoginActivity) getActivity();

        progress_bar = view.findViewById(R.id.progress_bar);
        bt_acceder = view.findViewById(R.id.bt_acceder);
        et_usuario = view.findViewById(R.id.et_usuario);
        et_password = view.findViewById(R.id.et_password);
        iv_settings = view.findViewById(R.id.iv_settings);
        iv_settings.setOnClickListener(this);

        progress_bar.setVisibility(View.GONE);

        bt_acceder.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        try{
            switch (v.getId()){
                case R.id.bt_acceder:
                    validarCredenciales();
                    break;
                case R.id.iv_settings:
                    main.active_fragment = new PreferenciasFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.main_container_login, main.active_fragment).commit();
                    break;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String task_id, final Response response) {
        main.runOnUiThread(new Runnable() {
            public void run() {
                progress_bar.setVisibility(View.GONE);

                if (response.isStatus() && !response.isError()){

                    if (response.getData() instanceof Usuario){
                        Usuario user_logged = (Usuario) response.getData();

                        Intent intent = new Intent(main.getApplicationContext(), MainActivity.class);
                        intent.putExtra("user_logged", user_logged);
                        startActivity(intent);
                    }else{
                        Toast.makeText(main.getApplicationContext(), "Credenciales no válidas", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(main.getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    /**
     * Valida las credenciales de acceso contra el servidor
     */
    public void validarCredenciales(){
        progress_bar.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(), "Validando credenciales...", Toast.LENGTH_SHORT).show();

        Usuario user = new Usuario();
        Request request = new Request();
        String usuario = et_usuario.getText().toString();
        String password = et_password.getText().toString();

        user.setUsuario(usuario);
        user.setPassword(password);

        request.setAction("validar-login");
        request.setData(user);
        ClientTask m = new ClientTask("validar-login", request, this, view.getContext());
        m.execute();
    }
}
