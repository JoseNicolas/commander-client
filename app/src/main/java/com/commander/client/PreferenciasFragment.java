package com.commander.client;

import android.preference.PreferenceFragment;
import android.os.Bundle;

public class PreferenciasFragment extends PreferenceFragment {

    public PreferenciasFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
