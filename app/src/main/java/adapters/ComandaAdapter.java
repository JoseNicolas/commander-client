package adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.commander.client.R;

import java.text.SimpleDateFormat;
import java.util.List;

import model.Comanda;
import model.DetalleComanda;
import model.Mesa;
import model.Producto;

public class ComandaAdapter extends RecyclerView.Adapter<ComandaAdapter.ComandaViewHolder > {
    private List<Comanda> lista_comandas;
    private List<Mesa> lista_mesas;

    public ComandaAdapter(List<Comanda> lista_comandas, List<Mesa> lista_mesas){
        this.lista_comandas = lista_comandas;
        this.lista_mesas = lista_mesas;
    }

    @NonNull
    @Override
    public ComandaAdapter.ComandaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ComandaViewHolder (LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comanda, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ComandaAdapter.ComandaViewHolder holder, int position) {
        String c_fecha = "";
        String c_estado = "";
        String c_mesa = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Comanda c = lista_comandas.get(position);
        Mesa mesa = lista_mesas.stream().filter(m -> m.getId() == c.getMesa()).findAny().get();

        c_fecha = String.valueOf( sdf.format(c.getCreated()) );
        c_estado = c.getEstado();
        c_mesa = mesa.getNombre();

        holder.tv_fecha.setText(c_fecha);
        holder.tv_estado.setText(c_estado);
        holder.tv_mesa.setText(c_mesa);

        switch (c_estado){
            case "pendiente":
                holder.iv_estado.setImageResource(R.drawable.ic_estado_pendiente);
                break;
            case "preparada":
                holder.iv_estado.setImageResource(R.drawable.ic_estado_preparada);
                break;
            case "servida":
                holder.iv_estado.setImageResource(R.drawable.ic_estado_servida);
                break;
            case "facturada":
                holder.iv_estado.setImageResource(R.drawable.ic_estado_facturada);
                break;
            default:
                holder.iv_estado.setImageResource(R.drawable.ic_estado_pendiente);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return this.lista_comandas.size();
    }


    public class ComandaViewHolder  extends RecyclerView.ViewHolder {
        TextView tv_fecha, tv_estado, tv_mesa;
        ImageView iv_estado;

        public ComandaViewHolder (View itemView){
            super(itemView);

            tv_fecha= itemView.findViewById(R.id.tv_fecha);
            tv_estado = itemView.findViewById(R.id.tv_estado);
            tv_mesa = itemView.findViewById(R.id.tv_mesa);
            iv_estado = itemView.findViewById(R.id.iv_estado);
        }
    }
}
