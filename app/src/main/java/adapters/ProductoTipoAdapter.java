package adapters;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.commander.client.MainActivity;
import com.commander.client.ProductoFragment;
import com.commander.client.R;

import java.util.List;

import model.ProductoTipo;


public class ProductoTipoAdapter extends RecyclerView.Adapter<ProductoTipoAdapter.ProductoTipoViewHolder > {
    MainActivity main;
    private List<ProductoTipo> lista_producto_tipo;

    public ProductoTipoAdapter(List<ProductoTipo> lista_producto_tipo){
        this.lista_producto_tipo = lista_producto_tipo;
    }

    @NonNull
    @Override
    public ProductoTipoAdapter.ProductoTipoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductoTipoViewHolder (LayoutInflater.from(parent.getContext()).inflate(R.layout.card_producto_tipos, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoTipoAdapter.ProductoTipoViewHolder holder, int position) {
        ProductoTipo pt = lista_producto_tipo.get(position);
        String pt_id = String.valueOf(pt.getId());
        String pt_text = "(" + pt.getId() + ") " + pt.getNombre();

        holder.tv_id.setText(pt_id);
        holder.tv_text.setText(pt_text);
    }

    @Override
    public int getItemCount() {
        return this.lista_producto_tipo.size();
    }

    public class ProductoTipoViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_text, tv_id;

        public ProductoTipoViewHolder (View itemView){
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_text = itemView.findViewById(R.id.tv_text);

            main = (MainActivity) itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Bundle bundle;
            TextView tv_producto_tipo;
            String id_producto_tipo_selected;
            Fragment producto_fragment;
            FragmentTransaction transaction;
            Toolbar toolbar;

            try{
                final CharSequence TITLE = "Seleccione Producto";
                toolbar = (Toolbar) main.findViewById(R.id.toolbar);
                toolbar.setTitle(TITLE);

                producto_fragment = new ProductoFragment();
                bundle = new Bundle();

                tv_producto_tipo = (TextView) v.findViewById(R.id.tv_id);
                id_producto_tipo_selected = (String) tv_producto_tipo.getText();

                bundle.putString("id_producto_tipo", id_producto_tipo_selected);
                producto_fragment.setArguments(bundle);

                MainActivity.active_fragment = producto_fragment;

                transaction = main.getFragmentManager().beginTransaction();
                transaction.replace(R.id.main_container, producto_fragment).commit();
            }
            catch (Exception ex){
                Toast.makeText(v.getContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
