package adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.commander.client.MainActivity;
import com.commander.client.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import model.DetalleComanda;
import model.Producto;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ProductoViewHolder > {
    private List<Producto> lista_productos;
    private List<Bitmap> lista_productos_images;

    public ProductoAdapter(List<Producto> lista_productos, List<Bitmap> lista_productos_images){
        this.lista_productos = lista_productos;
        this.lista_productos_images = lista_productos_images;
    }

    @NonNull
    @Override
    public ProductoAdapter.ProductoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductoViewHolder (LayoutInflater.from(parent.getContext()).inflate(R.layout.card_productos, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoAdapter.ProductoViewHolder holder, int position) {
        Producto p = lista_productos.get(position);
        String p_id = "(" + String.valueOf(p.getId()) + ")";
        String p_id_hidden = String.valueOf(p.getId());
        String p_nombre = p.getNombre();
        String p_precio = String.valueOf(p.getPrecio_unit()) + "€";
        Bitmap image = null;
        if (position < lista_productos_images.size()){
            image = lista_productos_images.get(position);
        }

        holder.tv_id.setText(p_id);
        holder.tv_id_hidden.setText(p_id_hidden);
        holder.tv_nombre.setText(p_nombre);
        holder.tv_precio.setText(p_precio);

        if (image != null){
            holder.iv_imagen.setImageBitmap(image);
        }
    }

    @Override
    public int getItemCount() {
        return this.lista_productos.size();
    }


    public class ProductoViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_nombre, tv_id, tv_precio, tv_id_hidden;
        ImageView iv_imagen;

        public ProductoViewHolder (View itemView){
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_id_hidden = itemView.findViewById(R.id.tv_id_hidden);
            tv_nombre = itemView.findViewById(R.id.tv_nombre);
            tv_precio = itemView.findViewById(R.id.tv_precio);
            iv_imagen = itemView.findViewById(R.id.iv_imagen);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(final View v) {

            final EditText input_cantidad;
            AlertDialog.Builder builder;
            final String producto_id;
            String producto_nombre;

            try{
                producto_id = tv_id_hidden.getText().toString();
                producto_nombre = tv_nombre.getText().toString();
                builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Cantidad: " + producto_nombre);

                input_cantidad = new EditText(v.getContext());
                input_cantidad.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                builder.setView(input_cantidad);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            if (MainActivity.detalle_comanda == null){
                                MainActivity.detalle_comanda = new ArrayList<>();
                            }

                            final String cantidad = input_cantidad.getText().toString();

                            if (cantidad != null){
                                if (!cantidad.equals("")){
                                    Optional<DetalleComanda> dc_exists = MainActivity.detalle_comanda.stream().filter(d -> String.valueOf(d.getProducto()) == producto_id).findAny();

                                    if (dc_exists.isPresent()){
                                        DetalleComanda dc = dc_exists.get();
                                        dc.setCantidad((dc.getCantidad()+Integer.parseInt(cantidad)));

                                    }else{
                                        DetalleComanda dc = new DetalleComanda();
                                        dc.setComanda(0);
                                        dc.setProducto(Integer.parseInt(producto_id));
                                        dc.setCantidad(Float.parseFloat(cantidad));
                                        dc.setCreated(new Date());
                                        dc.setCreator(MainActivity.user_logged.getId());
                                        dc.setUpdated(new Date());
                                        dc.setUpdater(MainActivity.user_logged.getId());

                                        MainActivity.detalle_comanda.add(dc);
                                    }
                                }else{
                                    Toast.makeText(v.getContext(), "No se añadió producto", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(v.getContext(), "No se añadió producto", Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();

            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
