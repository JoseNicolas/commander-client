package adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.commander.client.MainActivity;
import com.commander.client.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.DetalleComanda;
import model.Producto;

public class DetalleComandaAdapter extends RecyclerView.Adapter<DetalleComandaAdapter.DetalleComandaViewHolder > {
    private List<DetalleComanda> lista_detalle_comanda;
    private List<Producto> lista_productos;

    public DetalleComandaAdapter(List<DetalleComanda> lista_detalle_comanda, List<Producto> lista_productos){
        this.lista_detalle_comanda = lista_detalle_comanda;
        this.lista_productos = lista_productos;
    }

    @NonNull
    @Override
    public DetalleComandaAdapter.DetalleComandaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DetalleComandaViewHolder (LayoutInflater.from(parent.getContext()).inflate(R.layout.card_detalle_comanda, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DetalleComandaAdapter.DetalleComandaViewHolder holder, int position) {
        String p_id_hidden = "";
        String p_id = "";
        String p_nombre = "";
        String p_precio = "";
        String p_cantidad = "";


        DetalleComanda dc = lista_detalle_comanda.get(position);
        Producto p = lista_productos.stream().filter(prod -> prod.getId() == dc.getProducto()).findAny().get();

        if (p != null){
            p_id = String.valueOf( "(" + p.getId() + ")");
            p_id_hidden = String.valueOf( p.getId() );
            p_nombre = p.getNombre();
            p_cantidad = "x" + String.valueOf(dc.getCantidad());
            p_precio = String.valueOf(p.getPrecio_unit() + "€/U");
        }

        holder.tv_id.setText(p_id);
        holder.tv_id_hidden.setText(p_id_hidden);
        holder.tv_nombre.setText(p_nombre);
        holder.tv_precio.setText(p_precio);
        holder.tv_cantidad.setText(p_cantidad);

        holder.ib_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int id_producto;
                float cantidad;

                try{
                    String id_prodcuto_text = holder.tv_id.getText().toString();
                    String cantidad_text = holder.tv_cantidad.getText().toString();

                    id_prodcuto_text = id_prodcuto_text.replace("(", "");
                    id_prodcuto_text = id_prodcuto_text.replace(")", "");
                    id_producto = Integer.parseInt( id_prodcuto_text );

                    cantidad_text = cantidad_text.replace("x", "");
                    cantidad = Float.parseFloat(cantidad_text);

                    for (int i=0; i<lista_detalle_comanda.size(); i++){
                        DetalleComanda dc = lista_detalle_comanda.get(i);
                        if (dc.getProducto() == id_producto){
                            if (cantidad > 1){
                                dc.setCantidad((dc.getCantidad()-1));
                                holder.tv_cantidad.setText("x" + String.valueOf(dc.getCantidad()));
                            }else{
                                lista_detalle_comanda.remove(dc);

                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, lista_detalle_comanda.size());
                            }
                        }
                    }

                }catch (Exception e){
                    Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.lista_detalle_comanda.size();
    }


    public class DetalleComandaViewHolder  extends RecyclerView.ViewHolder {
        TextView tv_nombre, tv_id, tv_precio, tv_cantidad, tv_id_hidden;
        ImageButton ib_remove;

        public DetalleComandaViewHolder (View itemView){
            super(itemView);

            tv_id_hidden = itemView.findViewById(R.id.tv_id_hidden);
            tv_id = itemView.findViewById(R.id.tv_id);
            tv_precio = itemView.findViewById(R.id.tv_precio);
            tv_nombre = itemView.findViewById(R.id.tv_nombre);
            tv_cantidad = itemView.findViewById(R.id.tv_cantidad);
            ib_remove = itemView.findViewById(R.id.ib_remove);
        }
    }
}
