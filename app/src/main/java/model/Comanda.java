package model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Comanda implements Cloneable{
    // PROPIEDADES
    private int id;
    private int mesa;
    private int camarero;
    private String estado;
    private int factura;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public Comanda() {
    }

    public Comanda(int mesa, int camarero, String estado, int factura, Date created, int creator, Date updated, int updater) {
        this.mesa = mesa;
        this.camarero = camarero;
        this.estado = estado;
        this.factura = factura;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    public Comanda(int id, int mesa, int camarero, String estado, int factura, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.mesa = mesa;
        this.camarero = camarero;
        this.estado = estado;
        this.factura = factura;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public int getCamarero() {
        return camarero;
    }

    public void setCamarero(int camarero) {
        this.camarero = camarero;
    }
    
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getFactura() {
        return factura;
    }

    public void setFactura(int factura) {
        this.factura = factura;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }
    
    // METODOS

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.mesa);
        hash = 97 * hash + Objects.hashCode(this.camarero);
        hash = 97 * hash + Objects.hashCode(this.estado);
        hash = 97 * hash + Objects.hashCode(this.factura);
        hash = 97 * hash + Objects.hashCode(this.created);
        hash = 97 * hash + Objects.hashCode(this.creator);
        hash = 97 * hash + Objects.hashCode(this.updated);
        hash = 97 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comanda other = (Comanda) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.mesa, other.mesa)) {
            return false;
        }
        if (!Objects.equals(this.camarero, other.camarero)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.factura, other.factura)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Usuario camarero;
        Mesa mesa;
        try{
//            mesa = DaoFactory.getMesaDao().load(this.mesa);
            return "(" + this.id + ")" + " ["+this.getMesa()+"] ["+this.getCamarero()+"] " + sdf.format(this.created);
        }catch (Exception e){
            return "(" + this.id + ")" + " " + sdf.format(this.created);
        }
    }
    
    public String toStringValue() {
        return "Comanda{" + "id=" + id + ", mesa=" + mesa + ", camarero=" + camarero + ", estado=" + estado + ", factura=" + factura + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}
