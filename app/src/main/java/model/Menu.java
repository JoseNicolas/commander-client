package model;;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Menu implements Cloneable{
    // PROPIEDADES
    private int id;
    private String nombre;
    private String descripcion;
    private int[] productos;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public Menu() {
    }

    public Menu(String nombre, String descripcion, int[] productos, Date created, int creator, Date updated, int updater) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.productos = productos;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    public Menu(int id, String nombre, String descripcion, int[] productos, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.productos = productos;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int[] getProductos() {
        return productos;
    }

    public void setProductos(int[] productos) {
        this.productos = productos;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }
    
    // METODOS
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.id;
        hash = 41 * hash + Objects.hashCode(this.nombre);
        hash = 41 * hash + Objects.hashCode(this.descripcion);
        hash = 41 * hash + Objects.hashCode(this.productos);
        hash = 41 * hash + Objects.hashCode(this.created);
        hash = 41 * hash + Objects.hashCode(this.creator);
        hash = 41 * hash + Objects.hashCode(this.updated);
        hash = 41 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Menu other = (Menu) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.productos, other.productos)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "(" + this.id + ")" + " " + this.nombre;
    }
    
    public String toStringValue() {
        return "Menu{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", productos=" + productos + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}
