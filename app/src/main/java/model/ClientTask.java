package model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class ClientTask extends AsyncTask{

    private OnTaskCompleted taskCompleted;
    private final Request request;
    private final String task_id;
    private final int client_timeout;
    private final String server_ip_addr;
    private final int server_tcp_port;
    private SharedPreferences prefs;

    public ClientTask(String task_id, Request request, OnTaskCompleted taskCompleted, Context context){
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.taskCompleted = taskCompleted;
        this.request = request;
        this.task_id = task_id;
        client_timeout = Integer.parseInt(prefs.getString("string_timeout_servidor", "0"));
        server_ip_addr = prefs.getString("string_ip_servidor", null);
        server_tcp_port = Integer.parseInt(prefs.getString("string_puerto_servidor", "0"));

    }

    @Override
    protected Response doInBackground(Object[] objects) {
        final Client client;
        prefs.getString("string_ip_servidor", null);

        try{
            client = new Client();
            client.start();
            client.connect(client_timeout, server_ip_addr, server_tcp_port);
            client.addListener(new Listener() {
                public void received (Connection connection, Object object) {
                    if (object instanceof Response ){
                        Response r = (Response) object;
                        taskCompleted.onTaskCompleted(task_id, r);
                    }
                }
            });
            client.sendTCP(request);

        }catch (Exception e){
            e.printStackTrace();
            Response r = new Response();
            r.setError(true);
            r.setStatus(false);
            r.setMessage(e.getMessage());
            taskCompleted.onTaskCompleted(task_id, r);
        }

        return null;
    }

    public interface OnTaskCompleted{
        void onTaskCompleted(final String task_id, final Response response);
    }
}
