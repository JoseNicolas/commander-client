package model;

import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class ProductoTipo implements Cloneable{
    // PROPIEDADES
    private int id;
    private String nombre;
    private int padre;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public ProductoTipo() {
    }

    public ProductoTipo(String nombre, int padre, Date created, int creator, Date updated, int updater) {
        this.nombre = nombre;
        this.padre = padre;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    public ProductoTipo(int id, String nombre, int padre, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.nombre = nombre;
        this.padre = padre;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPadre() {
        return padre;
    }

    public void setPadre(int padre) {
        this.padre = padre;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }
    
    // METODOS
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.nombre);
        hash = 71 * hash + Objects.hashCode(this.padre);
        hash = 71 * hash + Objects.hashCode(this.created);
        hash = 71 * hash + Objects.hashCode(this.creator);
        hash = 71 * hash + Objects.hashCode(this.updated);
        hash = 71 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoTipo other = (ProductoTipo) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.padre, other.padre)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "("+this.getId()+") " + this.getNombre();
    }
    
    public String toStringValue() {
        return "ProductoTipo{" + "id=" + id + ", nombre=" + nombre + ", padre=" + padre + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}