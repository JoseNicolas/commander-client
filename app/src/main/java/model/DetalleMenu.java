package model;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/07 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class DetalleMenu implements Cloneable{
    // PROPIEDADES
    private int id;
    private int menu;
    private int producto;
    private int plato;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public DetalleMenu() {
    }

    public DetalleMenu(int menu, int producto, int plato, Date created, int creator, Date updated, int updater) {
        this.menu = menu;
        this.producto = producto;
        this.plato = plato;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    public DetalleMenu(int id, int menu, int producto, int plato, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.menu = menu;
        this.producto = producto;
        this.plato = plato;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMenu() {
        return menu;
    }

    public void setMenu(int menu) {
        this.menu = menu;
    }

    public int getProducto() {
        return producto;
    }

    public void setProducto(int producto) {
        this.producto = producto;
    }
    
    public int getPlato() {
        return plato;
    }

    public void setPlato(int plato) {
        this.plato = plato;
    }
  
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }
    
    // METHODS
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.menu);
        hash = 71 * hash + Objects.hashCode(this.producto);
        hash = 71 * hash + Objects.hashCode(this.plato);
        hash = 71 * hash + Objects.hashCode(this.created);
        hash = 71 * hash + Objects.hashCode(this.creator);
        hash = 71 * hash + Objects.hashCode(this.updated);
        hash = 71 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleMenu other = (DetalleMenu) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.menu, other.menu)) {
            return false;
        }
        if (!Objects.equals(this.producto, other.producto)) {
            return false;
        }
        if (!Objects.equals(this.plato, other.plato)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DetalleMenu{" + "id=" + id + ", menu=" + menu + ", producto=" + producto + ", plato=" + plato + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}
