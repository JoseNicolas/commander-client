package model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.List;

public class ClientTaskImage extends AsyncTask <Void, Void, Bitmap>{

    private ClientTaskImage.OnTaskImageCompleted taskImageCompleted;
    private final String server_ip_addr;
    private final int server_tcp_port;
    private String id_product_image;

    public ClientTaskImage(String id_product_image, OnTaskImageCompleted taskImageCompleted){
        this.id_product_image = id_product_image;
        this.taskImageCompleted = taskImageCompleted;
        server_ip_addr = "192.168.99.101";
        server_tcp_port = 13085;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Socket socket;
        InputStream is;
        Bitmap bitmap;

        try{
            socket = new Socket(server_ip_addr, server_tcp_port);

            //Send the message to the server
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            bw.write(id_product_image + "\n");
            bw.flush();

            // Get message from server
            is = socket.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            socket.close();

            if (bitmap != null){
                taskImageCompleted.onTaskImageCompleted(bitmap);
                return bitmap;
            }

        }catch (Exception e){
            e.printStackTrace();
            taskImageCompleted.onTaskImageCompleted(null);
            return null;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap image) {
        super.onPostExecute(image);

        int i = 0;
    }

    public interface OnTaskImageCompleted{
        void onTaskImageCompleted(final Bitmap bitmap);
    }
}
