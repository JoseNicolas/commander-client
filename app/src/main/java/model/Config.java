package model;

import android.os.Environment;

import java.io.FileReader;
import java.util.Properties;

/**
 * 2019/04/09
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Config {
    
    private static Properties getProperties() throws Exception{
        Properties properties = new Properties();
        
        try{
            String x = Environment.getExternalStorageDirectory().toString();
            properties.load(new FileReader( Environment.getExternalStorageDirectory().toString() + "/config.client.properties"));
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
        
        return properties;
    }
    
    public static String getProperty(String property) throws Exception{
        String host = "";
        
        try{
            Properties properties = getProperties();  
            host = properties.getProperty(property);
        }catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
        
        return host;
    }
}
