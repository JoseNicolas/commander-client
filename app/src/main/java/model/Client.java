package model;

import android.os.AsyncTask;
import android.util.Log;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/*
 * 2019/04/29
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Client extends com.esotericsoftware.kryonet.Client {

    private final int client_timeout;
    private final String server_ip_addr;
    private final int server_tcp_port;

    public Client(){
        super((3 * 1024), (3 * 1024));
        client_timeout = 10000;
        server_ip_addr = "192.168.99.101";
        server_tcp_port = 55555;

        this.registerAll(this);
    }

    /**
     * Serializa y añade las clases para su posterior uso.
     * Es necesario añadir todas las clases antes de realizar la conexion
     * @param client
     */
    public void registerAll(com.esotericsoftware.kryonet.Client client){

        client.getKryo().register(Date.class);
        client.getKryo().register(Request.class);
        client.getKryo().register(Response.class);
        client.getKryo().register(ArrayList.class);
        client.getKryo().register(Usuario.class);
        client.getKryo().register(Mesa.class);
        client.getKryo().register(Rol.class);
        client.getKryo().register(ProductoTipo.class);
        client.getKryo().register(Producto.class);
        client.getKryo().register(Menu.class);
        client.getKryo().register(Factura.class);
        client.getKryo().register(DetalleMenu.class);
        client.getKryo().register(DetalleComanda.class);
        client.getKryo().register(Comanda.class);
        client.getKryo().register(String[].class);
        client.getKryo().register(String.class);
        client.getKryo().register(Object[].class);
    }
}
