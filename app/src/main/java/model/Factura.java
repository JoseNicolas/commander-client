package model;

import java.util.Date;
import java.util.Objects;

/**
 * 2019/04/03 
 * @author José Francisco Nicolás Bautista
 * @version 1.0
 */
public class Factura implements Cloneable{
    // PROPIEDADES
    private int id;
    private String codigo;
    private Date fecha;
    private int comanda;
    private float importe;
    private boolean pagada;
    private String observaciones;
    private Date created;
    private int creator;
    private Date updated;
    private int updater;
    
    // CONSTRUCTORES
    public Factura() {
    }
    
    public Factura(String nombre, Date fecha, int comanda, float importe, boolean pagada, String observaciones, Date created, int creator, Date updated, int updater) {
        this.codigo = nombre;
        this.fecha = fecha;
        this.comanda = comanda;
        this.importe = importe;
        this.pagada = pagada;
        this.observaciones = observaciones;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }

    public Factura(int id, String nombre, Date fecha, int comanda, float importe, boolean pagada, String observaciones, Date created, int creator, Date updated, int updater) {
        this.id = id;
        this.codigo = nombre;
        this.fecha = fecha;
        this.comanda = comanda;
        this.importe = importe;
        this.pagada = pagada;
        this.observaciones = observaciones;
        this.created = created;
        this.creator = creator;
        this.updated = updated;
        this.updater = updater;
    }
    
    // GETTERS & SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }
    
    public int getComanda() {
        return comanda;
    }

    public void setComanda(int comanda) {
        this.comanda = comanda;
    }

    public boolean isPagada() {
        return pagada;
    }

    public void setPagada(boolean pagada) {
        this.pagada = pagada;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public Date getCreated() {
        return created;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public int getUpdater() {
        return updater;
    }

    public void setUpdater(int updater) {
        this.updater = updater;
    }
    
    // METODOS
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        hash = 67 * hash + Objects.hashCode(this.codigo);
        hash = 67 * hash + Objects.hashCode(this.fecha);
        hash = 67 * hash + Objects.hashCode(this.comanda);
        hash = 67 * hash + Objects.hashCode(this.importe);
        hash = 67 * hash + (this.pagada ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.observaciones);
        hash = 67 * hash + Objects.hashCode(this.created);
        hash = 67 * hash + Objects.hashCode(this.creator);
        hash = 67 * hash + Objects.hashCode(this.updated);
        hash = 67 * hash + Objects.hashCode(this.updater);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.comanda, other.comanda)) {
            return false;
        }
        if (!Objects.equals(this.importe, other.importe)) {
            return false;
        }
        if (this.pagada != other.pagada) {
            return false;
        }
        if (!Objects.equals(this.observaciones, other.observaciones)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!Objects.equals(this.updated, other.updated)) {
            return false;
        }
        if (!Objects.equals(this.updater, other.updater)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Factura{" + "id=" + id + ", codigo=" + codigo + ", fecha=" + fecha + ", comanda=" + comanda + ", importe=" + importe + ", pagada=" + pagada + ", observaciones=" + observaciones + ", created=" + created + ", creator=" + creator + ", updated=" + updated + ", updater=" + updater + '}';
    } 
    
    @Override
    public Object clone() throws CloneNotSupportedException{
        Object obj=null;
        obj=super.clone();
        return obj;
    }
}
